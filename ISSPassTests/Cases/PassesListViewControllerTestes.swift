//
//  ViewControllerTestes.swift
//  ISSPassTests
//
//  Created by nsn on 1/15/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import XCTest
@testable import ISSPass

class PassesListViewControllerTestes: XCTestCase {

    var passesListViewController: PassesListViewController!
    var iSSPass: ISSPass!

    override func setUp() {
        super.setUp()
        passesListViewController = PassesListViewController()
        iSSPass = ISSPass()

        let bundle = Bundle(for: PassesListViewControllerTestes.self)
        let path = bundle.path(forResource: "Test_ListOfPasses", ofType: "json")
        do {
            let data:NSData =  try NSData(contentsOfFile: path!)
            do {
                iSSPass = try JSONDecoder().decode(ISSPass.self, from: data as Data)
                self.passesListViewController.iSSPass = iSSPass
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }catch let jsonErr {
            print("Error in converting json file to Data", jsonErr)
        }
    }
    func testListHasExpectedItemCount(){

        XCTAssert(self.passesListViewController.iSSPass.response?.count == 6, "responce list do not have the expected number of items")
    }

    func testFirstItemHaveExpectedValue(){
        self.testItemHaveExpectedValueAtIndex(index: 0)
    }

    func testSecondItemHaveExpectedValue(){
        self.testItemHaveExpectedValueAtIndex(index: 1)
    }

    func testItemHaveExpectedValueAtIndex(index:Int){
        let rep = passesListViewController.iSSPass.response![index]
        let testRep = iSSPass.response![index]
        XCTAssertEqual(rep.duration, testRep.duration)
        XCTAssertEqual(rep.risetime, testRep.risetime)
    }

    func testTimeStampConvertedToCorrectReadableFormat(){
        XCTAssertEqual(passesListViewController.formatTimeStamp(stamp: 1516072104), "Jan 15 2018 09:08 PM")
    }

    override func tearDown() {
        super.tearDown()
    }
}
