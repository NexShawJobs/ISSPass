//
//  ViewController.swift
//  ISSPass
//
//  Created by nsn on 1/12/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {

    @IBOutlet weak var altitude: UITextField!
    @IBOutlet weak var passes: UITextField!
    @IBOutlet weak var latitude: UITextField!
    @IBOutlet weak var longitude: UITextField!
    @IBOutlet var viewModel: ViewModel!
    @IBOutlet weak var getPassesButton: UIButton!

    var locationManager: CLLocationManager!
    let defaultAltitude = 10
    let defaultPasses = 20

    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()

        altitude.delegate = self
        passes.delegate = self
        latitude.delegate = self
        longitude.delegate = self

        altitude.text = String(describing: defaultAltitude)
        passes.text = String (describing: defaultPasses)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: --
    //MARK: CLLocationManagerDelegate
    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if locations.count > 0 {
            let lat = locations[0].coordinate.latitude
            let lon = locations[0].coordinate.longitude
            latitude.text = String(lat)
            longitude.text = String(lon)
        }
    }

    func locationManager(_: CLLocationManager, didFailWithError: Error){

    }

     // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var characterSet:CharacterSet
        if (textField == altitude || textField == passes){
            characterSet = NSCharacterSet(charactersIn:"0123456789").inverted
        }else if (textField.text?.count == 0){
            characterSet = NSCharacterSet(charactersIn:"-0123456789").inverted
        }
        else if (textField.text?.count == 2 && textField == latitude){
            characterSet = NSCharacterSet(charactersIn:".0123456789").inverted
        }
        else if (textField.text?.count == 3 && textField == longitude){
            characterSet = NSCharacterSet(charactersIn:".0123456789").inverted
        }
        else{
            characterSet = NSCharacterSet(charactersIn:"0123456789").inverted
        }
        let compSepByCharInSet = string.components(separatedBy: characterSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
        //TO DO include all posible case

    }

    // MARK: - User Events
    @IBAction func getPasses(_ sender: Any) {
        let alert = UIAlertController(title: "Missing Required Value", message: "Latitude and Longitude are required values", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))

        var alt:String
        if altitude.text == ""{ alt = String(describing: defaultAltitude) } else { alt = altitude.text! }

        var pas:String
        if passes.text == "" { pas = String(describing: defaultPasses) } else { pas = passes.text! }

        var lat:String
        guard (latitude.text != nil) && latitude.text != "" else {
            self.present(alert, animated: true, completion: nil)
            return
        }
        lat = latitude.text!

        var lon:String
        guard (longitude.text != nil) && longitude.text != "" else {
            self.present(alert, animated: true, completion: nil)
            return
        }
        lon = longitude.text!

        viewModel.fetchISSPassesData(forAltitude: alt, latitude: lat, longitude: lon, passes: pas, completion: {

            DispatchQueue.main.async {
                let passesListViewController = self.storyboard?.instantiateViewController(withIdentifier: "passesListViewController") as! PassesListViewController
                passesListViewController.iSSPass = self.viewModel.iSSPass
                self.present(passesListViewController, animated: true, completion: nil)
            }
        })
        //TO DO include all posible error cases
        //Put all texts like messages and identifiers in one file like constants
    }
}

