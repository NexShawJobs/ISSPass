//
//  PassesListViewController.swift
//  ISSPass
//
//  Created by nsn on 1/13/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import UIKit

class PassesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var iSSPass:ISSPass = ISSPass()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (iSSPass.response?.count)!
    }


     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        if let duration = iSSPass.response![indexPath.row].duration{
            cell.textLabel?.text = String(describing: duration) + " sec"
        }
        if let risetime = iSSPass.response![indexPath.row].risetime{
            cell.detailTextLabel?.text =  self.formatTimeStamp(stamp: risetime)
        }
        return cell
    }

    // MARK: - Helper Methods
    func formatTimeStamp(stamp:Double) -> String{
        let date = NSDate(timeIntervalSince1970: stamp)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        return dayTimePeriodFormatter.string(from: date as Date)
    }

    // MARK: - User Events
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
