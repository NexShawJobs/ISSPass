//
//  ISSPassUITests.swift
//  ISSPassUITests
//
//  Created by nsn on 1/12/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import XCTest

class ISSPassUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testExample() {
        let app = XCUIApplication()
        let table =  app.tables.element(boundBy: 0)

        XCTAssertEqual(app.textFields.count, 4)
        XCTAssertEqual(app.buttons.count, 1)
        app.textFields["passes"].tap()
        app.textFields["passes"].typeText(getDeleteString(forTextFieldValue: (app.textFields["passes"].value as? String)!))
        app.textFields["passes"].typeText("25")

        app.textFields["altitude"].tap()
        app.textFields["altitude"].typeText(getDeleteString(forTextFieldValue: (app.textFields["altitude"].value as? String)!))
        app.textFields["altitude"].typeText("10000")

        app.textFields["latitude"].tap()
        app.textFields["latitude"].typeText(getDeleteString(forTextFieldValue: (app.textFields["latitude"].value as? String)!))
        app.textFields["latitude"].typeText("-37")

        app.textFields["longitude"].tap()
        app.textFields["longitude"].typeText(getDeleteString(forTextFieldValue: (app.textFields["longitude"].value as? String)!))
        app.textFields["longitude"].typeText("-180")

        app.buttons["Get List of Passes"].tap()
        XCTAssertEqual(table.cells.count, 25)
        app.buttons["Done"].tap()
    }

    func getDeleteString(forTextFieldValue value: String) ->String {
        return String(repeating: XCUIKeyboardKey.delete.rawValue, count: value.characters.count)
    }
}
